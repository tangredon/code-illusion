var target = Argument("target", "Default");
var configuration = Argument("configuration", "Debug");
var workingDir = MakeAbsolute(Directory("./"));
string artifactsDirName = "Artifacts";
string solutionFile = "./Illusion.sln";
string projectFile = "./Illusion/Illusion.csproj";
string netFrameworkString = "net461";
string netCoreString = "netcoreapp3.1";

string version = EnvironmentVariable("GitVersion_SemVer") ?? "0.0.0";

Setup(context =>
{
    Information($"Version: {version}");
});
Task("Info")
    .Does(() =>
    {
        Information(@"Cake build script starting...");
        
        Information(@"Requires C:\msys64 to be present for packaging (Pre-installed on AppVeyor) on Windows");
        Information(@"Working directory is: " + workingDir);
        if (IsRunningOnWindows())
        {
            Information("Platform is Windows");
        }
        else
        {
            Information("Platform is Linux, Windows builds will be skipped");
        }
    });
Task("Clean")
    .IsDependentOn("Info")
    .Does(() =>
    {
        CleanDirectories("./src/**/obj");
        CleanDirectories("./src/**/bin");
        CleanDirectories("./BuildOutput");
        CleanDirectories("./" + artifactsDirName);
        CreateDirectory("./" + artifactsDirName);
        Information("Clean completed");
    });

Task("Build")
    .IsDependentOn("Clean")
    .Does(() => {
        NuGetRestore("./Illusion.sln");
        var buildSettings = new MSBuildSettings()
            .SetConfiguration(configuration)
            .UseToolVersion(MSBuildToolVersion.VS2019);
        MSBuild(solutionFile, buildSettings);
});

Task("Publish-Framework")
	.IsDependentOn("Build")
	.Does(() => {
	    string buildOutputDir = "./BuildOutput/win-x64/Illusion";
        DotNetCorePublish(projectFile, netFrameworkString, "win-x64", buildOutputDir);
		Zip(buildOutputDir, $"./{artifactsDirName}/Illusion.Windows-{version}.zip");
	});

Task("Publish-Mono")
    .IsDependentOn("Build")
    .Does(() => {
        string buildOutputDir = "./BuildOutput/linux-x64/Illusion";
        DotNetCorePublish(projectFile, netFrameworkString, "linux-x64", buildOutputDir);
        CheckForGzipAndTar();
        Gzip("./BuildOutput/linux-x64", $"./{artifactsDirName}", "Illusion", $"Illusion.Mono-{version}.tar.gz");
    });

Task("Publish-Core")
    .IsDependentOn("Build")
    .Does(() => {
        string buildOutputDir = "./BuildOutput/win-x64/Illusion";
        DotNetCorePublish(projectFile, netCoreString, "win-x64", buildOutputDir);
        Zip(buildOutputDir, $"./{artifactsDirName}/Illusion.Core-{version}.zip");
    });

Task("Appveyor-Artifacts")
    .IsDependentOn("Clean")
    .Does(() =>
    {
        if (AppVeyor.IsRunningOnAppVeyor)
        {
            foreach (var file in GetFiles(workingDir + $"/{artifactsDirName}/*"))
            {
                AppVeyor.UploadArtifact(file.FullPath);
            }
        }
        else
        {
            Information(@"Skipping artifact push as not running in AppVeyor Windows Environment");
        }
    });

private void DotNetCorePublish(string projectPath, string framework, string runtime, string outputPath)
{
    // bool publishSingleFile = false;
    if (framework != netFrameworkString)
    {
        var settings = new DotNetCorePublishSettings
        {
            Framework = framework,
            Runtime = runtime,
            OutputDirectory = outputPath,
            ArgumentCustomization = args => args.Append("/p:PublishSingleFile=true")
        };
        DotNetCorePublish(projectPath, settings);
    }
    else
    {
        var settings = new DotNetCorePublishSettings
        {
            Framework = framework,
            Runtime = runtime,
            OutputDirectory = outputPath
        };
        DotNetCorePublish(projectPath, settings);
    }
}
private void RunMsysCommand(string utility, string utilityArguments)
{
    var msysDir = @"C:\msys64\usr\bin\";
    var utilityProcess = msysDir + utility + ".exe";
    Information("MSYS2 Utility: " + utility);
    Information("MSYS2 Directory: " + msysDir);
    Information("Utility Location: " + utilityProcess);
    Information("Utility Arguments: " + utilityArguments);
    IEnumerable<string> redirectedStandardOutput;
    IEnumerable<string> redirectedErrorOutput;
    var exitCodeWithArgument =
        StartProcess(
            utilityProcess,
            new ProcessSettings {
                Arguments = utilityArguments,
                WorkingDirectory = msysDir,
                RedirectStandardOutput = true
            },
            out redirectedStandardOutput,
            out redirectedErrorOutput
        );
    Information(utility + " output:" + Environment.NewLine + string.Join(Environment.NewLine, redirectedStandardOutput.ToArray()));
    // Throw exception if anything was written to the standard error.
    if (redirectedErrorOutput != null && redirectedErrorOutput.Any())
    {
        throw new Exception(
            string.Format(
                utility + " Errors ocurred: {0}",
                string.Join(", ", redirectedErrorOutput)));
    }
    Information(utility + " Exit code: {0}", exitCodeWithArgument);
}
private string RelativeWinPathToFullPath(string relativePath)
{
    return (workingDir + relativePath.TrimStart('.'));
}
private void RunLinuxCommand(string file, string arg)
{
    var startInfo = new System.Diagnostics.ProcessStartInfo()
    {
        Arguments = arg,
        FileName = file,
        UseShellExecute = true
    };
    var process = System.Diagnostics.Process.Start(startInfo);
    process.WaitForExit();
}
private void Gzip(string sourceFolder, string outputDirectory, string tarCdirectoryOption, string outputFileName)
{
    var tarFileName = outputFileName.Remove(outputFileName.Length - 3, 3);
    
    if (IsRunningOnWindows())
    {
        var fullSourcePath = RelativeWinPathToFullPath(sourceFolder);
        var tarArguments = @"--force-local -cvf " + fullSourcePath + "/" + tarFileName + " -C " + fullSourcePath + $" {tarCdirectoryOption} --mode ='755'";
        var gzipArguments = @"-k " + fullSourcePath + "/" + tarFileName;
        RunMsysCommand("tar", tarArguments);
        RunMsysCommand("gzip", gzipArguments);
        MoveFile($"{sourceFolder}/{tarFileName}.gz", $"{outputDirectory}/{tarFileName}.gz");
    }
    else
    {
        RunLinuxCommand("find",  MakeAbsolute(Directory(sourceFolder)) + @" -type d -exec chmod 755 {} \;");
        RunLinuxCommand("find",  MakeAbsolute(Directory(sourceFolder)) + @" -type f -exec chmod 644 {} \;");
        RunLinuxCommand("tar",  $"-C {sourceFolder} -zcvf {outputDirectory}/{tarFileName}.gz {tarCdirectoryOption}");
    }	
}
private void CheckForGzipAndTar()
{
    if (FileExists(@"C:\msys64\usr\bin\tar.exe") && FileExists(@"C:\msys64\usr\bin\gzip.exe"))
    {
        Information("tar.exe and gzip.exe were found");
    }
    else
    {
        throw new Exception("tar.exe and gzip.exe were NOT found");   
    }
}

Task("Dev")
    .Does(() => {
        Information("Dev Completed");
    });

Task("Linux")
    .Does(() => {
        Information("Linux Completed");
    });
Task("Windows")
    // .IsDependentOn("Publish-Core")
	.IsDependentOn("Publish-Framework")
    .IsDependentOn("Publish-Mono")
    .IsDependentOn("Appveyor-Artifacts")
    .Does(() => {
        Information("Windows Completed");
    });
Task("Default")
    .IsDependentOn("Dev")
    .Does(() => {
        Information("Default Task Completed");
    });

RunTarget(target);
